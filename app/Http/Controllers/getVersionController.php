<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class getVersionController extends Controller
{

    public function getVersion() {

        $version_list = array();

        $allVersion = DB::table('version')->select('version_name', 'date', 'status')->where('status', '=', 0)->get();
        // dd($allVersion);

        foreach ($allVersion as $versionName) {

            $version_list = array(
                'version_name' => $versionName->version_name,
            );
        }
       // dd($version_list);
        echo $version_list = json_encode($version_list);

        $allVersion = json_decode($version_list, true);


        foreach ($allVersion as $version_name) {

            //dd($version_name);

            if ($version_name == true) {

                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                    //Is it a proxy address
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }

                DB::table('server_details')->insert(
                    [
                        'version' => $version_name,
                        'ip' => $ip,
                        'date' => Carbon::now()
                    ]
                );

                $contents = $version_name . ' ' . $ip . ' ' . date('d/m/Y h:i:s A') . "\n";

                File::append('storage/log2.txt', $contents);
            }

        }
    }
}
