<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogUser;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('client');
    }


    public  function  index(Request $request){



        $userName = (\auth()->user())->name;

        $log = new LogUser();
        $log->set_table('log_'.$userName);

        $ipDetails = $log->where('adminid', $userName)->orderBy('id', 'desc')->first();

        //dd($ipDetails);
        if($ipDetails->signature == 0){

            $clientIP = $ipDetails->ip;

           // dd($clientIP);
            return view('admin.client', compact('clientIP'));

        }elseif($ipDetails->signature == 1){
            $syncClientIP = $ipDetails->ip;
           // dd($syncClientIP);
            return view('admin.client', compact('syncClientIP'));
        }

        return view('admin.client');
    }







}
