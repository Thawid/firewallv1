<?php

namespace App\Http\Controllers;

use App\UserLoginHistory;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use App\User;
use DB;
use App\LogUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use LiveControl\EloquentDataTable\DataTable;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* return view('admin/home');*/
        return view('admin.home');
    }

    public function allUser()
    {

        // return view('admin.allUser');
        $users = User::latest()->paginate(5);
        return view('admin.allUser', compact('users'));
    }

    public function create()
    {
        //
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatedData = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:40',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
        ]);
        if ($validatedData->fails()) {
            return redirect('admin/create')
                ->withErrors($validatedData)
                ->withInput();
        }
        $inputs = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => \Hash::make($request['password']),
        ];

        if (User::create($inputs)) {


            $userName = str_replace(' ', '_', $request['name']);
            Schema::create('log_' . $userName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('adminid');
                $table->string('ip')->nullable();
                $table->string('ip_history')->nullable();
                $table->string('log_ip_add_date')->nullable();
                $table->string('del_ip')->nullable();
                $table->string('log_ip_del_date')->nullable();
                $table->string('sip_ip')->nullable();
                $table->string('sip_ip_history')->nullable();
                $table->string('sip_ip_add_date')->nullable();
                $table->string('del_sip_ip')->nullable();
                $table->string('sip_delete_date')->nullable();
                $table->integer('status')->default('0');
                $table->integer('signature')->default('0');

                $table->timestamps();
            });


            return redirect("admin/allUser");

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::find($id);
        return view('admin.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        return view('admin.update', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = Validator::make($request->all(), [
            'name' => 'required|max:40',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        if ($validatedData->fails()) {
            return redirect('admin/create')
                ->withErrors($validatedData)
                ->withInput();
        }

        $inputs = [
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ];
        User::find($id)->update($inputs);

        return redirect("admin.allUser");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::find($id)->delete();

        return redirect("admin/allUser");

    }


    public function logHistory()
    {


        $userName = DB::table('users')->select('name')->get();


        //dd($userName);
        foreach ($userName as $user) {

            $data = DB::table('log_' . $user->name)->get();
            if ($data->count() > 0) {

                $loginDetails[] = $data;
            }

            $data = array_merge($loginDetails, compact('loginDetails'));


        }
        //dd($data);
        return view('admin.logHistory', compact('data'));
        //return view('admin.logHistory');

    }


    public function getAllUserLoginHistory(Request $request)
    {
        //  dd($request->all());
        $limit = $request->input('length', 10);
        $draw = $request->input('draw', 1);
        $search = $request->input('search')['value'];
        $offset = $request->input('start', 0);
        $userName = DB::table('users')->select('name')->get();


        foreach ($userName as $user) {
            $table = strtolower('log_' . $user->name);
            $logUser = DB::table('users')->leftJoin($table, 'users.name', '=', $table . ".adminid");
            $collection[] = $logUser->where('ip','!=',null)->where('ip','>',0)->get();

        }
        foreach ($collection as $userList) {
            foreach ($userList as $user) {
                $data[] = [
                    $user->name, $user->ip, $user->log_ip_add_date,
                ];
            }
        }
        return [
            'draw' => $draw,
            'data' => $data
        ];
    }

    public  function  homePageLoginHistory(Request $request){

        {
            //  dd($request->all());
            $limit = $request->input('length', 10);
            $draw = $request->input('draw', 1);
            $search = $request->input('search')['value'];
            $offset = $request->input('start', 0);
            $userName = DB::table('users')->select('name')->get();


            foreach ($userName as $user) {
                $table = strtolower('log_' . $user->name);
                $logUser = DB::table('users')->leftJoin($table, 'users.name', '=', $table . ".adminid")->take(5);
                $collection = $logUser->where('ip','!=',null)->where('ip','>',0);
                $collection[] = $logUser->take(5);
            }
            foreach ($collection as $userList) {
                foreach ($userList as $user) {
                    $data[] = [
                        $user->name, $user->ip, $user->log_ip_add_date,
                    ];
                }
            }
            return [
                'draw' => $draw,
                'data' => $data
            ];
        }
    }

}
