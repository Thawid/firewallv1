<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SipIpController extends Controller
{


    public function addSipIP()
    {

        return view('layouts.addSipIP');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storesipip(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'sip_ip' => 'required',

          ]);
        if ($validatedData->fails()) {
            return redirect('addSipIP')
                ->withErrors($validatedData)
                ->withInput();
        }

        $userName = DB::table('users')->select('name')->where('name', '=', auth()->user()->name)->first();

        //dd('log_'.strtolower($userName->name));

        $tableName = ('log_'.strtolower($userName->name));
        $ips = $request->input('sip_ip');
        foreach (explode(',',$ips) as $ip){

          $insert[] =  [
                'adminid' => $userName->name,
                'sip_ip' => $ip,
                'sip_ip_add_date' => Carbon::now(),
                'status' => 4,
            ];
        }
        DB::table($tableName)->insert($insert);

        //return view('layouts.addSipIP');
        return redirect('addSipIP')->with('message', 'SIP IP Successfully Added');


    }
}
