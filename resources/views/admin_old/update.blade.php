<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 4:00 PM
 */
?>


@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

    <h1>Dashboard</h1>

@stop




@section('content')
    <div class="container">
        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update User
                </div>
                <div class="panel-body">

                    {{ Form::model($user, ['url' => route('users.update',$user->id), 'method' => 'PUT' ]) }}
                    @include('admin._form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection




@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop



@section('js')
