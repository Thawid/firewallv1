<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 1:01 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 11:58 AM
 */
?>

@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

    <h1>Dashboard</h1>

@stop



@section('content')

    <div class="container">
        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add User
                </div>
                <div class="panel-body">
                    {{ Form::open(['url' => route('users.store'), 'method' => 'POST' ]) }}
                    @include('users._form')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@stop



@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop



@section('js')

    <script> console.log('Hi!'); </script>

@stop

