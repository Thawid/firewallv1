<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>->
            <!-- Optionally, you can add icons to the links -->
            <li>
                <a href="{{url('/home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>

            <li>
                <a href="{{url('loginDetails')}}">
                    <i class="fa fa-object-ungroup" aria-hidden="true"></i> <span>History</span>
                </a>
            </li>

           {{-- <li>
                <a href="{{url('addSipIP')}}">
                    <i class="fa fa-plus-square" aria-hidden="true"></i> <span>Add SIP IP</span>
                </a>
            </li>--}}

            <li @if(strpos(URL::current(), url('')) !== false)  class="treeview" @else
            class="treeview" @endif>
                <a href="javascript:void(0);">
                    <i class="fa fa-plus-square"></i>
                    <span>SIP IP</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(URL::current()==url('addSipIP')) class="active" @endif>
                        <a href="{{url('addSipIP')}}">
                            <i class="fa fa-circle-o" aria-hidden="true"></i> <span>Add SIP IP</span>
                        </a>
                    </li>
                    <li @if(URL::current()==url('UserAllSIP')) class="active" @endif>
                        <a href="{{url('UserAllSIP')}}"><i class="fa fa-circle-o"></i> <span>All SIP IP</span></a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="{{url('logout')}}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> <span>Sign Out</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
