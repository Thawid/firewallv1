<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/26/2018
 * Time: 6:07 PM
 */
?>


@extends('layouts.master')

@section('title',"CloudCoder || Firewall")
@section('style')
    <link rel="stylesheet" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Client Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Login Details</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <table class="table table-hover table-bordered" id="table">
                <thead>

                <th> SL</th>
                <th> IP</th>
                <th> Login Date</th>
                <th>Action</th>

                </thead>

            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection


@section('script')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script> console.log('Hi!'); </script>
    <script>
        //$(".table").dataTable();
    </script>
    <script>
        $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '{{route('getLogDetails')}}',
                "type": "get"
            },
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            },
        });
    </script>
@endsection