<?php
/**
 * Created by Arifur Rahman.
 * Date: 10/30/2018
 * Time: 6:44 PM
 */
?>
    <!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('web/favicon.ico')}}"/>
    @include('layouts.include.header')
    <title>@yield('title')</title>
    @yield('style')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
{{--include the top menu bar--}}
@include('layouts.include.top_sidebar')
{{--include the left side bar--}}
@include('layouts.include.left_sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="col-md-3">
                @yield('header_left')
            </h1>
            <h3 class="col-md-5 text-center no-margin">@yield('header_title')</h3>
            <ol class="breadcrumb">
                @yield('header_right')
            </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>

            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        <!-- Small boxes (Stat box) -->
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{--include the footer--}}
    @include('layouts.include.footer')
</div>
@yield('modal')
<!-- ./wrapper -->
{{--link the js plugin--}}
@include('layouts.include.javascript_bar')
@yield('script')
</body>
</html>

