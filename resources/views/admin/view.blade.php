<?php
/**
 * Created by PhpStorm.
 * User: tanvi
 * Date: 11/18/2018
 * Time: 3:49 PM
 */
?>

@extends('admin.layout.master')

@section('title',"CloudCoder || Firewall")
@section('style')

    <link rel="stylesheet" href="/css/admin_custom.css">
@endsection
@section('header_left')
    Dashboard
    <small>Admin Dashboard</small>
@endsection

@section('header_right')
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
@endsection

@section('content')

        @if(session()->has('status'))
            <p class="alert alert-info">
                {{  session()->get('status') }}
            </p>
        @endif
            <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{--<a href="{{ route('users.index') }}" class="btn btn-success btn-xs">Back</a> --}}
                    User Details
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Name</label>
                            <div class="col-sm-9">
                                <p class="form-control">{{ $user->name }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Email</label>
                            <div class="col-sm-9">
                                <p class="form-control">{{ $user->email }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Password</label>
                            <div class="col-sm-9">
                                <p class="form-control">********</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Created On</label>
                            <div class="col-sm-9">
                                <p class="form-control">{{ $user->created_at->format('m-d-Y') }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">Last Updated</label>
                            <div class="col-sm-9">
                                <p class="form-control">{{ $user->created_at->format('m-d-Y') }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


@endsection

@section('script')
    <script> console.log('Hi!'); </script>
@endsection
