<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');



Route::resource('users', 'Admin\UserController');

Auth::routes();

Route::get('home', 'HomeController@index', function(){})->name('home');
Route::get('admin/login', 'AdminController@index', function(){})->name('admin.login');

Route::any('admin/logout', 'Admin\LoginController@logout')->name('admin');

Route::get('logout', 'Auth\LoginController@logout');



Route::GET('admin/home','AdminController@index');
Route::GET('admin/client','ClientController@index');

Route::GET('admin/allUser','AdminController@allUser');
Route::GET('update/{id}','AdminController@edit', function(){})->name('admin.update');
Route::GET('view/{id}','AdminController@show', function(){})->name('admin.view');
Route::post('destroy/{id}','AdminController@destroy')->name('admin.destroy');
Route::get('admin/create','AdminController@create')->name('admin.create');
Route::post('admin/store','AdminController@store')->name('admin.store');

Route::get('admin/addVersion','VersionController@addVersion')->name('admin.addVersion');
Route::post('admin/storeVersion','VersionController@storeVersion')->name('admin.storeVersion');
Route::POST('deleteVersion/{id}','VersionController@deleteVersion')->name('admin.deleteVersion');

Route::get('getVersion', 'getVersionController@getVersion')->name('getVersion');

//Route::GET('admin/client/api','ApiController@index');

Route::get('admin/logHistory', 'AdminController@logHistory')->name('logHistory');
Route::get('admin/getAllUserLoginHistory', 'AdminController@getAllUserLoginHistory')->name('getAllUserLoginHistory');
Route::get('admin/homePageLoginHistory', 'AdminController@getAllUserLoginHistory')->name('homePageLoginHistory');

Route::get('loginDetails', 'HomeController@UserLoginHistory')->name('loginDetails');

Route::get('getLogDetails', 'HomeController@getLogDetails')->name('getLogDetails');
Route::get('homeLogDetails', 'HomeController@homeLogDetails')->name('homeLogDetails');

Route::get('UserAllSIP', 'HomeController@UserAllSIP')->name('UserAllSIP');

Route::get('UserAllSIPip', 'HomeController@UserAllSIPip')->name('UserAllSIPip');

Route::get('deleteIP/{id}','HomeController@deleteIP')->name('layouts.deleteIP');
Route::GET('admin','Admin\LoginController@showLoginForm')->name('admin.login');
Route::GET('addSipIP','SipIpController@addSipIP')->name('addSipIP');
Route::post('storesipip','SipIpController@storesipip')->name('storesipip');
Route::get('getIPStatus', 'HomeController@getIPStatus')->name('getIPStatus');

Route::get('deleteSIPIP/{id}','HomeController@deleteSIPIP')->name('layouts.deleteSIPIP');

Route::POST('admin','Admin\LoginController@login');

Route::POST('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::POST('admin-password/reset','Admin\ResetPasswordController@reset');
Route::GET('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
